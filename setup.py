import sys
import setuptools
import nuitka_setuptools

with open('Readme.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

packages = setuptools.find_packages(exclude=('tests'))

cmdclass = {'build_ext': nuitka_setuptools.Nuitka}

build_settings = dict(cmdclass=cmdclass, ext_modules=nuitka_setuptools.Compile(packages)) if any('bdist' in arg for arg in sys.argv) else {}

setuptools.setup(
    name='perceptron',
    version='0.0.0',
    author='shadowlegend',
    author_email='legend@shadowlegend.me',
    description='Simple Perceptron Implementation',
    long_description=readme,
    long_description_content_type='text/markdown',
    license=license,
    include_package_data=True,
    packages=packages,
    **build_settings
)