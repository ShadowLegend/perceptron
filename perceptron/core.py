import numpy

class Perceptron():
    __value = 0.0
    __total_sum = 0.0

    def __init__(self, inputs, weights):
        self.__total_sum = (inputs * weights[1:]).sum() + weights[0]
        self.__value = (1 / (1 + numpy.exp(-self.__total_sum)))

    @property
    def total_sum(self):
        return self.__total_sum
    
    @property
    def value(self):
        return self.__value