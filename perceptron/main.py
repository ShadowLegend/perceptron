import os
import sys

sys.path.append('/Users/shadowlegend/projects/perceptron')

import numpy
import time
import matplotlib
import sklearn.datasets as sklearn_datasets
import sklearn.preprocessing as sklearn_preprocessing

import perceptron.core as perceptron_core

threshold = 10000
training_data_len_as_percent = 80
bias = 1.0
learning_rate = 0.1

def run():
    breast_cancer_dataset = sklearn_datasets.load_breast_cancer()

    training_data_len = int(breast_cancer_dataset.data.shape[0] * training_data_len_as_percent / 100)

    training_datas = sklearn_preprocessing.minmax_scale(breast_cancer_dataset.data[:training_data_len])
    training_targets = breast_cancer_dataset.target[:training_data_len]

    weights = numpy.random.uniform(low=0.0, high=1.0, size=(training_datas[0].shape[0] + 1))

    error_record = open('error_record.txt', 'w')

    # begin fitting
    for _ in range(threshold):
        for i in range(training_data_len):
            predict = perceptron_core.Perceptron(training_datas[i], weights)
            weights[0] += learning_rate * (training_targets[i] - perceptron_core.linear_interceptor_sigmoid_derivative(predict.value))
            weights[1:] += learning_rate * (training_targets[i] - perceptron_core.linear_slope_sigmoid_derivative(training_datas[i], predict.total_sum, predict.value))
    # end fitting

    error_record.close()

    test_datas = sklearn_preprocessing.minmax_scale(breast_cancer_dataset.data[training_data_len:])

    print('\n======= PREDICTION ======')
    test_predictions = [perceptron_core.Perceptron(test_data, weights).value for test_data in test_datas]
    print(test_predictions)
    print(len(test_predictions))
    print('======= /PREDICT ======')

    print('\n====== TARGET ======')
    test_targets = breast_cancer_dataset.target[training_data_len:]
    print(test_targets)
    print(test_targets.shape[0])
    print('====== /TARGET ======')

if __name__ == '__main__':
    run()
